import * as acm from "@aws-cdk/aws-certificatemanager";
import * as cdk from "@aws-cdk/core";
import * as cloudfront from "@aws-cdk/aws-cloudfront";
import * as ec2 from "@aws-cdk/aws-ec2";
import * as ecs from "@aws-cdk/aws-ecs";
import * as ecsPattern from "@aws-cdk/aws-ecs-patterns";
import * as elasticache from "@aws-cdk/aws-elasticache";
import * as elb from "@aws-cdk/aws-elasticloadbalancingv2";
import * as iam from "@aws-cdk/aws-iam";
import * as rds from "@aws-cdk/aws-rds";
import * as route53 from "@aws-cdk/aws-route53";
import * as s3 from "@aws-cdk/aws-s3";
import * as secretsmanager from "@aws-cdk/aws-secretsmanager";
import * as targets from "@aws-cdk/aws-route53-targets";
import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as waf from "@aws-cdk/aws-wafv2";

export class ThroatStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const base = this.node.tryGetContext("baseDns") ?? "phuks.co";
    const subname = this.node.tryGetContext("subname") ?? ""; // Such as www...
    const databaseName: string =
      this.node.tryGetContext("databaseName") ?? "phuks";
    const databaseUsername: string =
      this.node.tryGetContext("databaseUsername") ?? "phuks";
    const cert: string | void = this.node.tryGetContext("cert");

    const adminKey = this.node.tryGetContext("admin");
    const appKey = this.node.tryGetContext("appKey");

    // Set up S3 bucket
    const bucket = new s3.Bucket(this, "uploads", {
      bucketName: `${subname}-throat`,
      accessControl: s3.BucketAccessControl.PRIVATE,
    });

    const backup = new s3.Bucket(this, "backup", {
      bucketName: `${subname}-backup-ovarit`,
      accessControl: s3.BucketAccessControl.PRIVATE,
    });

    const logBucket = new s3.Bucket(this, "logs", {
      bucketName: `${subname}-logs-ovarit`,
      accessControl: s3.BucketAccessControl.PRIVATE,
    });

    new cdk.CfnOutput(this, "s3_bucket", { value: bucket.bucketName });
    new cdk.CfnOutput(this, "s3_bucket_region", {
      value: bucket.bucketRegionalDomainName,
    });

    // Set up VPC
    const vpc = new ec2.Vpc(this, "TheVPC", {
      enableDnsHostnames: false,
      natGateways: 0,
      subnetConfiguration: [
        { name: "public", subnetType: ec2.SubnetType.PUBLIC },
        { name: "isolated", subnetType: ec2.SubnetType.ISOLATED },
      ],
    });

    const sg1 = new ec2.SecurityGroup(this, "sg1", {
      vpc,
      allowAllOutbound: true,
    });

    sg1.connections.allowFrom(
      ec2.Peer.ipv4(vpc.vpcCidrBlock),
      ec2.Port.allTcp()
    );
    const securityGroups = [
      ec2.SecurityGroup.fromSecurityGroupId(
        this,
        "default-vpc",
        vpc.vpcDefaultSecurityGroup
      ),
      sg1,
    ];

    const subnetGroup = new elasticache.CfnSubnetGroup(
      this,
      "redis-subnet-group",
      {
        cacheSubnetGroupName: `${subname.replace(".", "-")}-${base.replace(
          ".",
          "-"
        )}-redissubnetgroup2`,
        subnetIds: [...vpc.privateSubnets, ...vpc.isolatedSubnets].map(
          (s) => s.subnetId
        ),
        description: "VPC Private Subnets",
      }
    );

    subnetGroup.node.addDependency(vpc);

    // Add redis
    const redis = new elasticache.CfnCacheCluster(this, "redis-cluster", {
      engine: "redis",
      cacheNodeType: "cache.t3.micro",
      numCacheNodes: 1,
      cacheSubnetGroupName: subnetGroup.cacheSubnetGroupName,
      vpcSecurityGroupIds: securityGroups.map((sg) => sg.securityGroupId), // Even money this doesn't work.
    });

    redis.node.addDependency(sg1);
    redis.addDependsOn(subnetGroup);

    const dbSubnetGroup = new rds.CfnDBSubnetGroup(this, "db-subnet-group", {
      dbSubnetGroupName: `${subname.replace(".", "-")}-${base.replace(
        ".",
        "-"
      )}-db-subnet-group`,
      subnetIds: [...vpc.privateSubnets, ...vpc.isolatedSubnets].map(
        (s) => s.subnetId
      ),
      dbSubnetGroupDescription: `VPC Private Subnets - DB ${subname}`,
    });
    const dbPassword = new secretsmanager.Secret(
      this,
      `${subname}-db-password`,
      {
        generateSecretString: {
          includeSpace: false,
          excludeCharacters: '/@"',
        },
      }
    );

    const database = new rds.CfnDBCluster(this, "serverless-db", {
      engine: "aurora-postgresql",
      engineMode: "serverless",
      databaseName,
      masterUsername: databaseUsername,
      masterUserPassword: dbPassword.secretValue.toString(),
      scalingConfiguration: {
        autoPause: true,
        minCapacity: 2,
        maxCapacity: 16,
        secondsUntilAutoPause: 300,
      },
      dbSubnetGroupName: dbSubnetGroup.dbSubnetGroupName,
      vpcSecurityGroupIds: securityGroups.map((sg) => sg.securityGroupId),
      storageEncrypted: true,
    });

    database.node.addDependency(sg1);
    database.addDependsOn(dbSubnetGroup);
    database.node.addDependency(dbPassword);

    const role = new iam.Role(this, "throat-role", {
      assumedBy: new iam.ServicePrincipal("ecs-tasks.amazonaws.com"),
      inlinePolicies: {
        xray: new iam.PolicyDocument({
          statements: [
            new iam.PolicyStatement({
              effect: iam.Effect.ALLOW,
              resources: ["*"],
              actions: ["xray:*"],
            }),
          ],
        }),
      },
    });

    bucket.grantReadWrite(role);
    backup.grantReadWrite(role);

    const cluster = new ecs.Cluster(this, "throat-cluster-new", {
      vpc,
    });

    const zone = route53.HostedZone.fromLookup(this, "primary-zone", {
      domainName: base,
    });

    const cfnDomainName = `${subname}.${base}`;
    const uploadsDomainName =
      subname === "www" ? `uploads.${base}` : `uploads.${cfnDomainName}`;

    const certArn: string =
      cert != null
        ? cert
        : new acm.DnsValidatedCertificate(this, "cft-dist-cert", {
            domainName: cfnDomainName,
            hostedZone: zone,
            region: "us-east-1",
          }).certificateArn;

    const uploadCertArn =
      cert != null
        ? cert
        : new acm.DnsValidatedCertificate(this, "cft-upload-dist-cert", {
            domainName: uploadsDomainName,
            hostedZone: zone,
            region: "us-east-1",
          }).certificateArn;

    const oai = new cloudfront.OriginAccessIdentity(this, "oai", {
      comment: `Uploads OAI for ${cfnDomainName}`,
    });

    const cookieSecret = new secretsmanager.Secret(this, "cookie-secret");
    const emailSecret = new secretsmanager.Secret(this, "smtp-password");

    const environment = {
      APP_REDIS_URL: `redis://${redis.attrRedisEndpointAddress}:${redis.attrRedisEndpointPort}`,
      //APP_SERVER_NAME: `*.${base}`,
      CACHE_TYPE: "redis",
      CACHE_REDIS_URL: `redis://${redis.attrRedisEndpointAddress}:${redis.attrRedisEndpointPort}`,
      DATABASE_ENGINE: "playhouse.pool.PooledPostgresqlDatabase",
      DATABASE_STALE_TIMEOUT: "300",
      DATABASE_MAX_CONNECTIONS: "300",
      BACKUP_BUCKET: backup.bucketName,
      STORAGE_PROVIDER: "S3_US_WEST_OREGON",
      STORAGE_CONTAINER: bucket.bucketName,
      STORAGE_THUMBNAILS_PATH: bucket.bucketName,
      STORAGE_THUMBNAILS_URL: `https://${uploadsDomainName}/`,
      STORAGE_UPLOADS_PATH: bucket.bucketName,
      STORAGE_UPLOADS_URL: `https://${uploadsDomainName}/`,
      AWS_CONFIG_FILE: ".awsconfig",
      MAIL_SERVER: "email-smtp.us-west-2.amazonaws.com",
      MAIL_PORT: "587",
      MAIL_USERNAME: "AKIAUYCRZB7DJIPXG45E",
      MAIL_DEFAULT_FROM: `no-reply@${base}`,
      DATABASE_USER: databaseUsername,
      DATABASE_HOST: database.attrEndpointAddress,
      DATABASE_PORT: database.attrEndpointPort,
      DATABASE_NAME: databaseName,
      AUTH_PROVIDER: "KEYCLOAK",
      AUTH_KEYCLOAK_SERVER: this.node.tryGetContext("keyserver")!,
      AUTH_KEYCLOAK_ADMIN_REALM: "master",
      AUTH_KEYCLOAK_ADMIN_CLIENT: "admin-cli",
      AUTH_KEYCLOAK_USER_REALM: this.node.tryGetContext("keyuser")!,
      AUTH_KEYCLOAK_AUTH_CLIENT: this.node.tryGetContext("keyclient")!,
      SUBNAME: subname,
      //AWS_XRAY_DAEMON_ADDRESS: "xray-daemon:2000",
    };
    const secrets = {
      MAIL_PASSWORD: ecs.Secret.fromSecretsManager(emailSecret),
      APP_SECRET_KEY: ecs.Secret.fromSecretsManager(cookieSecret),
      DATABASE_PASSWORD: ecs.Secret.fromSecretsManager(dbPassword),
      AUTH_KEYCLOAK_ADMIN_SECRET: ecs.Secret.fromSecretsManager(
        secretsmanager.Secret.fromSecretArn(this, "adminKey", adminKey)
      ),
      AUTH_KEYCLOAK_AUTH_SECRET: ecs.Secret.fromSecretsManager(
        secretsmanager.Secret.fromSecretArn(this, "authKey", appKey)
      ),
    };

    /*
     * auth:
  # Set to LOCAL to store user authentication in database,
  # or KEYCLOAK to use a Keycloak server to authenticate users.
  provider: 'LOCAL'

  # Set to True to require users to provide valid email addresses.
  require_valid_emails: False

  keycloak:
    # URL of the Keycloak server's REST API.
    server: 'http://auth.example.com/auth/'

    # Keycloak realm containing client with power to create users.
    admin_realm: 'master'

    # Keycloak client with power to create users.
    admin_client: 'admin-cli'

    # Keycloak realm for users.
    user_realm: 'example'

    # Keycloak client in user realm with authorization enabled.
    auth_client: 'throatapp'

    # Client credentials grant secrets
    admin_secret: '00000000-0000-0000-0000-000000000000' # secret for admin client
    auth_secret: '00000000-0000-0000-0000-000000000000' # secret for auth client
*/

    const image = ecs.ContainerImage.fromAsset("../throat/");
    const migration = new ecs.ContainerDefinition(this, "migration-container", {
      image,
      taskDefinition: new ecs.FargateTaskDefinition(this, "migration-task", {
        taskRole: role,
      }),
      logging: ecs.LogDriver.awsLogs({
        streamPrefix: "migration",
      }),
      environment,
      secrets,
      command: ["./scripts/migrate.py"],
    });

    const ecsStack = new ecsPattern.ApplicationMultipleTargetGroupsFargateService(
      this,
      "ecs",
      {
        cluster,
        assignPublicIp: true,
        desiredCount: subname === "www" ? 2 : 1,
        //publicLoadBalancer: true,
        healthCheckGracePeriod: cdk.Duration.minutes(15),
        targetGroups: [
          {
            containerPort: 5001,
            priority: 1,
            pathPattern: "/socket.io*",
          },
          {
            containerPort: 5000,
          },
        ],
        taskImageOptions: {
          taskRole: role,
          image,
          family: ec2.InstanceClass.BURSTABLE3,
          environment,
          secrets,
          containerPorts: [5000, 5001],
          enableLogging: true,
        },
      }
    );

    ecsStack.targetGroup.setAttribute("stickiness.enabled", "true");

    ecsStack.targetGroup.configureHealthCheck({
      path: "/health",
    });

    ecsStack.loadBalancer.logAccessLogs(logBucket, "ALB");

    //const xray = ecsStack.taskDefinition.addContainer("xray-daemon", {
    //  image: ecs.ContainerImage.fromRegistry("amazon/aws-xray-daemon"),
    //  essential: false,
    //});

    //xray.addPortMappings({ containerPort: 2000, protocol: ecs.Protocol.UDP });

    const acl = new waf.CfnWebACL(this, "alb-acl", {
      defaultAction: {
        allow: {},
      },
      scope: "REGIONAL",
      visibilityConfig: {
        cloudWatchMetricsEnabled: true,
        sampledRequestsEnabled: true,
        metricName: "throat-alb-acl",
      },
      rules: [
        // {
        //   name: "throat-alb-ipv4",
        //   visibilityConfig: {
        //     cloudWatchMetricsEnabled: true,
        //     sampledRequestsEnabled: true,
        //     metricName: "throat-alb-acl-ipv4",
        //   },
        //   overrideAction: { none: {} },
        //   priority: 0,
        //   statement: {
        //     ipSetReferenceStatement: {
        //       arn: ip_block.attrArn,
        //     },
        //   },
        // },
        //        {
        //          name: "throat-from-cloudfront",
        //          visibilityConfig: {
        //            cloudWatchMetricsEnabled: true,
        //            sampledRequestsEnabled: true,
        //            metricName: "throat-from-cloudfront",
        //          },
        //          overrideAction: { none: {} },
        //          priority: 0,
        //          statement: {
        //            byteMatchStatement: {
        //              positionalConstraint: "EXACTLY",
        //              textTransformations: [
        //                {
        //                  priority: 0,
        //                  type: "NONE",
        //                },
        //              ],
        //              searchString: "C005DED0-5CD5-4322-8866-1063C12ADD96",
        //              fieldToMatch: {
        //                singleHeader: { name: "x-waf-alb" },
        //              },
        //            },
        //          },
        //        },
        //{
        //  name: "throat-alb-AWSManagedRulesCommonRuleSet",
        //  visibilityConfig: {
        //    cloudWatchMetricsEnabled: true,
        //    sampledRequestsEnabled: true,
        //    metricName: "throat-alb-acl-AWSManagedRulesCommonRuleSet",
        //  },
        //  overrideAction: { none: {} },
        //  priority: 1,
        //  statement: {
        //    managedRuleGroupStatement: {
        //      name: "AWSManagedRulesCommonRuleSet",
        //      vendorName: "AWS",
        //    },
        //  },
        //},
        {
          name: "throat-alb-AWSManagedRulesAmazonIpReputationList",
          visibilityConfig: {
            cloudWatchMetricsEnabled: true,
            sampledRequestsEnabled: true,
            metricName: "throat-alb-acl-AWSManagedRulesAmazonIpReputationList",
          },
          priority: 2,
          overrideAction: { none: {} },
          statement: {
            managedRuleGroupStatement: {
              name: "AWSManagedRulesAmazonIpReputationList",
              vendorName: "AWS",
            },
          },
        },
        {
          name: "throat-alb-AWSManagedRulesLinuxRuleSet",
          visibilityConfig: {
            cloudWatchMetricsEnabled: true,
            sampledRequestsEnabled: true,
            metricName: "throat-alb-acl-AWSManagedRulesLinuxRuleSet",
          },
          priority: 3,
          overrideAction: { none: {} },
          statement: {
            managedRuleGroupStatement: {
              name: "AWSManagedRulesLinuxRuleSet",
              vendorName: "AWS",
            },
          },
        },
        {
          name: "throat-alb-AWSManagedRulesKnownBadInputsRuleSet",
          visibilityConfig: {
            cloudWatchMetricsEnabled: true,
            sampledRequestsEnabled: true,
            metricName: "throat-alb-acl-AWSManagedRulesKnownBadInputsRuleSet",
          },
          priority: 4,
          overrideAction: { none: {} },
          statement: {
            managedRuleGroupStatement: {
              name: "AWSManagedRulesKnownBadInputsRuleSet",
              vendorName: "AWS",
            },
          },
        },
      ],
    });

    new waf.CfnWebACLAssociation(this, "waf-alb-ass", {
      webAclArn: acl.attrArn,
      resourceArn: ecsStack.loadBalancer.loadBalancerArn,
    });

    const scaling = ecsStack.service.autoScaleTaskCount({
      maxCapacity: 4,
      minCapacity: subname === "www" ? 2 : 1,
    });
    scaling.scaleOnCpuUtilization("CpuScaling", {
      targetUtilizationPercent: 75,
      scaleInCooldown: cdk.Duration.seconds(60),
      scaleOutCooldown: cdk.Duration.seconds(60),
    });
    ecsStack.node.addDependency(redis);

    // Create domain name & CloudFront distribution
    const cfUploadsDistro = new cloudfront.CloudFrontWebDistribution(
      this,
      "uploads-distribution",
      {
        aliasConfiguration: {
          acmCertRef: uploadCertArn,
          names: [uploadsDomainName],
          sslMethod: cloudfront.SSLMethod.SNI,
          securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2018,
        },
        originConfigs: [
          {
            s3OriginSource: {
              s3BucketSource: bucket,
              originAccessIdentity: oai,
            },
            behaviors: [
              {
                minTtl: cdk.Duration.days(7),
                defaultTtl: cdk.Duration.days(7),
                maxTtl: cdk.Duration.days(100),
                allowedMethods: cloudfront.CloudFrontAllowedMethods.GET_HEAD,
                compress: true,
                isDefaultBehavior: true,
              },
            ],
          },
        ],
      }
    );

    new route53.ARecord(this, "cfn-uploads-ARecord", {
      zone,
      recordName: uploadsDomainName,
      target: route53.RecordTarget.fromAlias(
        new targets.CloudFrontTarget(cfUploadsDistro)
      ),
    });
    new route53.AaaaRecord(this, "cfn-uploads-AaaaRecord", {
      zone,
      recordName: uploadsDomainName,
      target: route53.RecordTarget.fromAlias(
        new targets.CloudFrontTarget(cfUploadsDistro)
      ),
    });

    const domainNames = [cfnDomainName];
    if (subname == "www") {
      domainNames.push(base);
    }

    const cfdistro = new cloudfront.CloudFrontWebDistribution(
      this,
      "distribution",
      {
        defaultRootObject: "/",
        aliasConfiguration: {
          acmCertRef: certArn,
          names: domainNames,
          sslMethod: cloudfront.SSLMethod.SNI,
          securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2018,
        },
        loggingConfig: {
          bucket: logBucket,
          prefix: "application-cf",
          includeCookies: false,
        },
        originConfigs: [
          {
            originHeaders: {
              "x-waf-alb": "C005DED0-5CD5-4322-8866-1063C12ADD96",
            },
            customOriginSource: {
              domainName: ecsStack.loadBalancer.loadBalancerDnsName,
              originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
            },
            behaviors: [
              {
                minTtl: cdk.Duration.seconds(0),
                defaultTtl: cdk.Duration.seconds(1),
                maxTtl: cdk.Duration.seconds(1),
                allowedMethods: cloudfront.CloudFrontAllowedMethods.ALL,
                compress: true,
                isDefaultBehavior: true,
                forwardedValues: {
                  queryString: true,
                  headers: ["Host", "X-CSRFToken"],
                  cookies: { forward: "all" },
                },
              },
              {
                pathPattern: "static*",
                defaultTtl: cdk.Duration.hours(20),
                maxTtl: cdk.Duration.hours(24),
              },
            ],
          },
        ],
      }
    );

    domainNames.forEach((name) => {
      new route53.ARecord(
        this,
        domainNames.length == 1 ? "cfn-ARecord" : `${name}-cfn-ARecord`,
        {
          zone,
          recordName: name,
          target: route53.RecordTarget.fromAlias(
            new targets.CloudFrontTarget(cfdistro)
          ),
        }
      );
      new route53.AaaaRecord(
        this,
        domainNames.length == 1 ? "cfn-AaaaRecord" : `${name}-cfn-AaaaRecord`,
        {
          zone,
          recordName: name,
          target: route53.RecordTarget.fromAlias(
            new targets.CloudFrontTarget(cfdistro)
          ),
        }
      );
    });

    const dashboard = new cloudwatch.Dashboard(this, "throat-heads-up", {
      dashboardName: `${subname}_Throat_Heads_Up`,
      periodOverride: cloudwatch.PeriodOverride.AUTO,
    });

    // cluster?
    dashboard.addWidgets(
      new cloudwatch.GraphWidget({
        title: "CPU - Throat",
        left: [cluster.metric("CpuUtilization"), cluster.metric("CpuReserved")],
      }),
      new cloudwatch.GraphWidget({
        title: "RAM - Throat",
        left: [
          cluster.metric("MemoryUtilization"),
          cluster.metric("MemoryReserved"),
        ],
      }),
      new cloudwatch.GraphWidget({
        title: "Containers - Throat",
        left: [cluster.metric("ServiceCount"), cluster.metric("TaskCount")],
      })
    );
    // ALB
    dashboard.addWidgets(
      new cloudwatch.GraphWidget({
        title: "ALB Requests",
        left: [ecsStack.loadBalancer.metricRequestCount()],
      }),
      new cloudwatch.GraphWidget({
        title: "ALB Target Status Codes",
        left: [
          ecsStack.loadBalancer.metricHttpCodeTarget(
            elb.HttpCodeTarget.TARGET_2XX_COUNT
          ),
          ecsStack.loadBalancer.metricHttpCodeTarget(
            elb.HttpCodeTarget.TARGET_3XX_COUNT
          ),
          ecsStack.loadBalancer.metricHttpCodeTarget(
            elb.HttpCodeTarget.TARGET_4XX_COUNT
          ),
          ecsStack.loadBalancer.metricHttpCodeTarget(
            elb.HttpCodeTarget.TARGET_5XX_COUNT
          ),
        ],
      }),
      new cloudwatch.GraphWidget({
        title: "Throat Latency",
        left: [ecsStack.loadBalancer.metricTargetResponseTime()],
      })
    );
    dashboard.addWidgets(
      new cloudwatch.GraphWidget({
        title: "WAF",
        left: [
          new cloudwatch.Metric({
            namespace: "WAF",
            metricName: "BlockedRequests",
          }),
          new cloudwatch.Metric({
            namespace: "WAF",
            metricName: "AllowedRequests",
          }),
        ],
      }),
      new cloudwatch.GraphWidget({
        title: "DDOS",
        left: [
          new cloudwatch.Metric({
            namespace: "DDoSProtection",
            metricName: "DDoSDetected",
          }),
        ],
      })
    );
  }
}
